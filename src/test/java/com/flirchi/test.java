package com.flirchi;

import org.junit.*;
import static com.flirchi.Resources.*;
import static com.flirchi.PageElements.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Anton Slyvka on 19-Sep-15.
 */
public class test extends DriverFirefox {

    @Before
    public void start() {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @After
    public void end() {
        driver.quit();
    }

    @Test
    public void registerTest() {
        openPage(startURL);
        verifyPageURL(startURL);
        preselect(femaleSearch, femaleHairWhitish, femaleEyesDark, femaleFigureSvelte);
        verifyElementOnPage(femaleSearchResults);
        enterValue(femaleNameField, userName);
        //issue is present - there is no validation for "space" between names, but assert is writing "Please enter name without spaces"
        enterValue(femaleEmailField, userEmail);
        selectFromDropdown(femaleSearchGenderDropdown, maleGender);
        selectFromDropdown(femaleSearchAgeDropdown, age);
        clickOnElement(submitMaleButton);
        verifyElementOnPage(registeredUserCard);


    }

}
