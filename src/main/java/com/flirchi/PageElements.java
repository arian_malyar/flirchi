package com.flirchi;

/**
 * Created by Anton Slyvka on 19-Sep-15.
 */
public class PageElements {
    public static String startURL = "https://flirchi.com/sign/inter?fr=1&p=1232";
    //female preselect
    public static String femaleSearch = "#slide-1>.select-prefer>.select-button-left";
    public static String femaleHairDark = "#slide-2.f>.select-prefer>.select-button-left";
    public static String femaleHairWhitish = "#slide-2.f>.select-prefer>.select-button-right";
    public static String femaleEyesDark = "#slide-3.f>.select-prefer>.select-button-left";
    public static String femaleEyesWhitish = "#slide-3.f>.select-prefer>.select-button-right";
    public static String femaleFigureNormal = "#slide-4.f>.select-prefer>.select-button-left";
    public static String femaleFigureSvelte = "#slide-4.f>.select-prefer>.select-button-right";
    public static String femaleSearchResults = "#slide-6.f";
    //male preselect
    public static String maleSearch = "#slide-1>.select-prefer>.select-button-right";
    public static String maleHairDark = "#slide-2.f>.select-prefer>.select-button-left";
    public static String maleHairWhitish = "#slide-2.f>.select-prefer>.select-button-right";
    public static String maleEyesDark = "#slide-3.f>.select-prefer>.select-button-left";
    public static String maleEyesWhitish = "#slide-3.f>.select-prefer>.select-button-right";
    public static String malefigureNormal = "#slide-4.f>.select-prefer>.select-button-left";
    public static String malefigureSvelte = "#slide-4.f>.select-prefer>.select-button-right";
    public static String maleSearchResults = "#slide-6.m";
    //female register form
    public static String femaleNameField = "#form-signup-f>#field-name>#form-signup-name";
    public static String femaleEmailField = "#form-signup-f>#field-email>#form-signup-email";
    public static String femaleSearchGenderDropdown = "#form-signup-f>.land-form-row>#field-gender>#field-gender-select";
    public static String femaleSearchAgeDropdown = "#form-signup-f>.land-form-row>#field-age>#field-age-select";
    //male register form
    public static String maleNameField = "#form-signup-m>#field-name>#form-signup-name";
    public static String maleEmailField = "#form-signup-m>#field-email>#form-signup-email";
    public static String maleSearchGenderDropdown = "#form-signup-m>.land-form-row>#field-gender>#field-gender-select";
    public static String maleSearchAgeDropdown = "#form-signup-m>.land-form-row>#field-age>#field-age-select";
    //user data
    public static String userName = "firstName lastName";
    public static String userEmail = "email4@mailinator.com";
    public static String femaleGender = "f";
    public static String maleGender = "m";
    public static String age = "18";

    public static String submitMaleButton = "#submit_sign_up_f";
    public static String submitFemaleButton = "#submit_sign_up_m";
    public static String registeredUserCard = ".prof-card-info";
}
