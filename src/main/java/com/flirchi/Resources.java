package com.flirchi;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import static org.junit.Assert.assertTrue;

/**
 * Created by Anton Slyvka on 19-Sep-15.
 */
public class Resources extends DriverFirefox {

    public static void openPage(String url){
        driver.get(url);
    }
    public static void pageDelay(int timeout) {
        try {
            Thread.sleep(timeout);
        }
        catch (Exception e) {
            System.out.println("pageDelay catch error " + e);
        }
    }
    public static void clickOnElement(String element) {
        driver.findElement(By.cssSelector(element)).click();
    }
    public static void preselect(String lookFor, String hairColor, String eyeColor, String figure) {
        clickOnElement(lookFor);
        clickOnElement(hairColor);
        clickOnElement(eyeColor);
        clickOnElement(figure);
        pageDelay(5000);
    }
    public static void verifyElementOnPage(String element) {
        assertTrue(driver.findElement(By.cssSelector(element)).isDisplayed());
    }
    public static void verifyPageURL(String element) {
        assertTrue(driver.getCurrentUrl().equals(element));
    }
    public static void enterValue(String element, String value) {
        driver.findElement(By.cssSelector(element)).sendKeys(value);
    }
    public static void selectFromDropdown(String element, String value) {
        new Select(driver.findElement(By.
                cssSelector(element))).selectByValue(value);
    }






}
